var bodyParser = require('body-parser');

// can be deprecated if we use rejectUnauthorized: true in the transport.js
function checkClientAuthorization(req,res,next){
	// if (!req.client.authorized) {
	//      var e = new Error('Unauthorized: Client certificate required ' + 
	//                    '(' + req.client.authorizationError + ')');
	//      e.status = 401;
	//      return next(e);
	//    }
	//    next();
	var cert = req.connection.getPeerCertificate();
	console.log(cert);
	if(!req.client.authorized)
		res.status(401).end('Unauthorized: Client certificate required. '+req.connection.authorizationError);
	else
		next();
}

module.exports.setup = function(app){

    app.use(bodyParser());

    app.get('/api/secret', checkClientAuthorization, function(req, res) {
        var n = {message: 'secret'};
        res.json(n);
    });
}