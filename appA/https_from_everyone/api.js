var bodyParser = require('body-parser');

module.exports.setup = function(app){

    app.use(bodyParser());

    app.get('/api/public', function(req, res) {
        var n = {message: 'public'};
        res.json(n);
    });
}