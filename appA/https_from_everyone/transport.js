var https = require('https');
var fs = require('fs');

var opts = {
  // Server SSL private key and certificate
  key: fs.readFileSync('../server.key'),
  cert: fs.readFileSync('../server.crt'),
  // issuer/CA certificate against which the client certificate will be
  // validated. A certificate that is not signed by a provided CA will be
  // rejected at the protocol layer.
  ca: fs.readFileSync('../ca.crt'),
  // request a certificate, but don't necessarily reject connections from
  // clients providing an untrusted or no certificate. This lets us protect only
  // certain routes, or send a helpful error message to unauthenticated clients.
  // requestCert: true,
  rejectUnauthorized: false
};

module.exports.setup = function(app) {
    var p = 4000;

    var a = https.createServer(opts,app);
    a.listen(p, function() {
        console.log('appA accessible from everyone on port %d', a.address().port);
    });
}