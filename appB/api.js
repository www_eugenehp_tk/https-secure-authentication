var https = require('https');
var fs = require("fs");

module.exports.setup = function(app){
    app.get('/callAppA_public', function(eReq, eRes) {

        var options = {
            host: 'server.eugenehp.tk',
            port: 4000,
            path: '/api/public/',
            method: 'GET',
            // key: fs.readFileSync("../userA.key"),
            // cert: fs.readFileSync("../userA.crt"),
            ca: fs.readFileSync("../ca.crt")
        };

        var req = https.request(options, function(res) {
            console.log("statusCode: ", res.statusCode);
            console.log("headers: ", res.headers);

            var response = "";
            console.log('appB > callAppA_public > ');

            res.on('data', function(d) {
                // process.stdout.write(d);
                response +=d;
            });

            res.on('end',function(end){
                eRes.set('Content-Type', res.headers['contnet-type']);
                eRes.end(response);
            });
            
        });

        req.end();

        req.on('error', function(e) {
            console.error(e);
        });
    });

    app.get('/callAppA_secret', function(eReq, eRes) {    
        var options = {
            host: 'server.eugenehp.tk',
            port: 4001,
            path: '/api/secret/',
            method: 'GET',
            key: fs.readFileSync("../userA.key"),
            cert: fs.readFileSync("../userA.crt"),
            ca: fs.readFileSync("../ca.crt")
        };

        var req = https.request(options, function(res) {
            console.log("statusCode: ", res.statusCode);
            console.log("headers: ", res.headers);

            var response = "";
            console.log('appB > callAppA_secret > ');

            res.on('data', function(d) {
                // process.stdout.write(d);
                response +=d;
            });

            res.on('end',function(end){
                eRes.set('Content-Type', res.headers['contnet-type']);
                eRes.end(response);
            });
            
        });

        req.end();

        req.on('error', function(e) {
            console.error(e);
        });
    });
}