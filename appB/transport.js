var http = require('http');

module.exports.setup = function(app) {
    var p = 4002;

    appB = http.createServer(app);
    appB.listen(p, function() {
        console.log('appB on port %d', appB.address().port);
    });
}