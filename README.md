DISCLAIMER: I'm not into https terminology, I make mess with words:
            sometime I can mix 'certificate', 'key', etc

What I want to achieve is to have 2 different https connection.
One with a public certificate,
One with a private certificate.

- everyone can access to the public api via https to localhost:4000/api/public
  when you open the browser on https://localhost:4000/api/public
  the browser will show the yello/res page that there is an untrusted cretificate
  agree on it, you can visit the page

- only appB (with the right key-cert files signed with the same certificate authority file)
  can access to https://localhost:4001/api/secret.
  All other connection (without the right key) has to be rejected with some 'Unauthorized' message.

- test all the thing with appB:
  (of course those two appB rest api/endpoint are only for testing porpouse)
  curl http://localhost:4002/callAppA_public shoud give {message: 'public'}
  curl http://localhost:4002/callAppA_secret shoud give {message: 'secret'}

- test with the browser:
  open the page https://loalhost:4000/api/public has to give a red/yellow page asking to trust the certificate
  open the page https://loalhost:4001/api/secret has to give an Unathorized message

- test with curl:
  curl -k https://loalhost:4000/api/public has to give {message: 'public'}
  curl --some_cool_option_using_appB_key_cert_files https://loalhost:4001/api/secret has to give {message: 'secret'}

- of course a generate_all_keys.sh that will generate:
  1. https server key-cert files for https://loalhost:4000/api/public
  2. https server key-cert files for https://loalhost:4001/api/secret
  3. https client key-cert files for accessing https://loalhost:4001/api/secret